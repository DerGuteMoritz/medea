(cond-expand
  (chicken-4 (import medea) (use test))
  (chicken-5 (import medea test)))

(define-syntax test-read/write
  (syntax-rules ()
    ((_ desc doc)
     (test desc doc (read-json
                     (with-output-to-string 
                         (lambda ()
                           (write-json doc))))))))

(test-group "writing"
  (test-write "{}" '())
  (test-write "[]" '#())
  (test-error (json-write 'a))
  (test-write "{\"foo\":[123]}" '((foo . #(123))))
  (test-write "{\"foo\":null,\"bar\":true}" '((foo . null) (bar . #t)))
  (test-write "[1,2,[3],4,{}]" '#(1 2 #(3) 4 ()))
  (test-write "[\"\\\\\"]" '#("\\"))
  (test-write "[\"Дҫ\"]" '#("Дҫ")))


(test-group "writing and reading"
  (test-read/write "simple" '#(1 2 3 "foo" ((bar . null) (baz))))
  (test-read/write "python pass 1" (read-json json-python-test-pass1)))
